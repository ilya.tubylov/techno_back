const PORT = 9001

const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const {secret} = require("./config")
const URL_DB = "mongodb+srv://admin:1334@cluster0.csdgt.mongodb.net/?retryWrites=true&w=majority"
const Users = require('./models/Users')
const Post = require('./models/Posts')

const app = express()

const news = {
    header: 'New 1',
    content: 'Компью́тер (англ. computer, МФА: [kəmˈpjuː.tə(ɹ)] — «вычислитель», от лат. computare — считать, вычислять) — термин, пришедший в русский язык из иноязычных (в основном — английских) источников, одно из названий электронной вычислительной машины. Используется в данном смысле в русском литературном языке, научной, научно-популярной литературе.',
    imgage: ''
}

const generateAccessToken = (id) => {
    const payload = {
        id
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"} )
}

// указание статичной папки
app.use(express.static(__dirname + "/static"))
app.use(cors())
app.use(express.json())

app.get('/news', (req, res) => {
    res.json(news)
})
app.get('/page', (req, res) => {
    res.send('<h1>Страница новостей</h1>')
})
app.post('/registration', async (req, res) => {
    console.log(req.body)
    const {login, password, email} = req.body
    let candidate = await Users.findOne({login})
    if (candidate) {
        return res.status(400).json({message: "Данный пользователь уже зарегистрирован"})
    }
    const user = new Users({login, password, email})
    await user.save()
    res.json({
        login: login,
        password: password,
        email: email
    })
})
app.post('/login', async (req, res) => {
    console.log(req.body)
    const {login, password} = req.body
    let candidate = await Users.findOne({login})
    if (!candidate) {
        return res.status(400).json({message: "Данный пользователь не зарегистрирован"})
    }
    if(candidate.password !== password){
        return res.status(400).json({message: "Неверный пароль"})
    }
    const token = generateAccessToken(candidate._id)
    res.json({
        login: login,
        password: password,
        token: token
    })
})
app.get('/', (req, res) => {
    res.sendFile('index.html')
})

const start = async () => {
    try {
        await mongoose.connect(URL_DB, { useNewUrlParser: true, useUnifiedTopology: true, authSource:"admin" })
        app.listen(PORT, () => console.log(`Сервер запущен на ${PORT} порте`))
    } catch (e) {
        console.log(e)
    }
}

start()